//creamos la clase que se encargara de interactuar con nuestros botones y pantalla
class Display {
    //creamos nuestro constructor y pasamos propiedades del display
    constructor(valoranteriornumeros, valoractualnumeros) {
        this.valoractualnumeros = valoractualnumeros;
        this.valoranteriornumeros = valoranteriornumeros;
        this.calculator = new Calculadora();
        this.tipoOperacion = undefined;
        this.valorActual = '';
        this.valorAnterior = '';
        //mapa para mostrar nuestros signos de operadores
        this.signos = {
            sumarnumeros: '+',
            restarnumeros: '-',
            multiplicarnumeros: 'X',
            dividirnumeros: '/',
            porcentajenumeros: '%'
        }
    }

    //creamos nuestro metodo de borrar

    borrar() {
        this.valorActual = this.valorActual.toString().slice(0, -1);
        this.imprimirNumeros();
    }

    //metodo de borrar todo 

    borartodosnumeros() {
        this.valorActual = '';
        this.valorAnterior = '';
        this.tipoOperacion = undefined;
        this.imprimirNumeros();

    }

    //recibe nuestro tipo de operacion
computar(tipo) {
    //si nuestro tipo de operacion es diferente o distinto de igual calculamos
    this.tipoOperacion !== 'igual' && this.calcular();
    this.tipoOperacion = tipo;
    this.valorAnterior = this.valorActual || this.valorAnterior;
    this.valorActual = '';
    this.imprimirNumeros();
}
    //recibimos nuestros numeros y los mostramos
    agregarNumero(numero) {
        //Nuestra condicion if nos permite agregar solo un punto
        if (numero === '.' && this.valorActual.includes('.')) return
        this.valorActual = this.valorActual.toString() + numero.toString();
        this.imprimirNumeros();
    }

  
    //metodo que nos permite la impresion de nuestros numeros
    imprimirNumeros() {
        this.valoractualnumeros.textContent = this.valorActual;
        this.valoranteriornumeros.textContent = `${this.valorAnterior} ${this.signos[this.tipoOperacion] || ''}`;
    }

    calcular() {
    
        const valorAnterior = parseFloat(this.valorAnterior);
        const valorActual = parseFloat(this.valorActual);

        //hacemos condicion por si no tenemos nada

        if( isNaN(valorActual) || isNaN(valorAnterior)) return
        this.valorActual = this.calculator[this.tipoOperacion](valorAnterior, valorActual);

    }

}