//creación de nuestras constantes para recibir los ids de nuestros botones, numeros y operaciones
const valoranteriornumeros = document.getElementById('valor-anterior');
const valoractualnumeros = document.getElementById('valor-actual');
const botonesNumeros = document.querySelectorAll('.numeros');
const botonesOperadores = document.querySelectorAll('.operadores');
const display = new Display(valoranteriornumeros, valoractualnumeros);
//hacemos un for each y funcion
botonesNumeros.forEach(boton => {
    //recibimos los numeros que estemos presionando
    boton.addEventListener('click', () => {
        display.agregarNumero(boton.innerHTML);
    });
})

//seleccionamos todos nuestros botones que son de operaciones

botonesOperadores.forEach(boton => {
    //computar es nuestra funcion 
    boton.addEventListener('click', () => display.computar(boton.value));
})