var personArr = [
    {
        "personId" : 123,
        "name" : "Jhon",
        "city" : "Melbourne",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 124,
        "name" : "Amelia",
        "city" : "Sydney",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 125,
        "name" : "AmeEmilylia",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 126,
        "name" : "Abraham",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    }
]

//Aquí el codgo de todo. 

let botondatos = document.querySelector('button');
let tabla = document.querySelector('#tabladatos');

let headers = ['personId', 'name', 'city', 'phoneNo'];

botondatos.addEventListener('click', () => {
    let tabladatos = document.createElement('tabledatos');
    let headerRow = document.createElement('tr');

    headers.forEach(headerText => {
        let header = document.createElement('th');
        let textNode = document.createTextNode(headerText);
        header.appendChild(textNode);
        headerRow.appendChild(header);
    });
    tabladatos.appendChild(headerRow);

    personArr.forEach(emp => {

        let row = document.createElement('tr');
        Object.values(emp).forEach(text => {
            let cell = document.createElement('td');
            let textNode = document.createTextNode(text);
            cell.appendChild(textNode);
            row.appendChild(cell);
        })
        tabladatos.appendChild(row);

    });
    tabla.appendChild(tabladatos);
    
});