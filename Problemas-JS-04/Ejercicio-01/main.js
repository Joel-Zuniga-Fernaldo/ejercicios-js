// Dado el siguiente objeto y arreglo

var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0': 'zero!',
    '1': 'one'
};

var anArray = ['zero', 'one.'];


//Cual es el resultado del siguiente código y porque?


//Nos imprime la posisión 0 del arreglo y lo que tenemos en nuestro objeto en 0, quedando zero sero!
console.log(anArray[0], anObject[0]); 

/*Nos imprime one. one esto debido a que es lo que tenemos en valor 1 de nuestro objeto 
y lo que tenemos en la posicion 1 de nuestro arreglo.*/
console.log(anArray[1], anObject[1]); 

/*Nos da como resultado 2, interesting  debido a que en nuestro arreglo esta contando los elementos
 que tenemos que son dos en este caso, y e nuestro objeto nos devuelve lo que tiene en esa propiedad lenght */
console.log(anArray.length, anObject.length); 

/*En este caso nos deolvera un undefinido en el arreglo, debido a que 
no existe una propiedad foo en el, mientras que en nuestro objeto sí 
contamos con la propeidad de foo que a su vez tiene algo que devolvernos un string */
console.log(anArray.foo, anObject.foo);

/*Usamos nuestro operador typeof, el cual en este caso estamos diciendo
que si nuestro arreglo es literal igual a un arreglo nos devolvera vedadeero
pero esto pasa si so si porque en javascript los arreglo son considerados objetos, y pues 
nuestro objeto si es un arreglo entonces por eso nos devuelve un true true */
console.log(typeof anArray == 'object', typeof anObject == 'object');


/*En este caso nuestro operador instanceof esta diciendo que si adelante de nuestra variable
tenemos  un objeto, nos va devolver verdadero en este caso devido a que
javascript lo toma como objeto si o si, y pues el anobject es verdadero porque si es de 
tipo objeto */
console.log(anArray instanceof Object, anObject instanceof Object);


/*Aqui estamos diciendo si adelante d nuestro anArray tenemos un arreglo o es de tipo array 
nos devolvera verdadero o falso en este caso verdadero porque es un arreglo, 
y en nuestro anObject sera un falso ya que es un objeto y no es de tipo Array */

console.log(anArray instanceof Array, anObject instanceof Array);

/* En esta parte usamos el metodo Array.isArray para determinar si el valor que estaremos 
pasando es un arreglo o no, en este caso la respuesta es obvia true false, devido 
a que anArray si es arreglo y anObject no es arreglo si no un objeto */
console.log(Array.isArray(anArray), Array.isArray(anObject));