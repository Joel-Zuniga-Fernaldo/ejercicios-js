//Dado el siguiente código:

let zero = 0;
function multiply(x) {return x * 2;}
function add(a = 1 + zero, b = a, c = b+a, d = multiply(c)) {
    console.log((a + b + c), d);
}


add(1); //toma el valor de add, en este caso 1, 1+1 que es c, dando a 2 pasandolo a multipli que lo multiplica x2 por eso sale 4, 4
add(3); //toma el valor  de 3 mismo procedimiento de arriba
add(2, 7); //Nos da  18 y 18
add(1, 2, 5); //nos manda 8 y 10
add(1, 2, 5, 10); //de la misma manera 8 y 10