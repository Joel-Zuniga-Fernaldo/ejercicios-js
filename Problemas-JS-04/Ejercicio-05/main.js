//Dado el siguiente código

class MyClass {
    constructor() {
        this.names_ = [];
    }

    set name(value) {
        this.names_.push(value);
    }

    get name() {
        return this.names_[this.names_.length -1];
    }
}


const MyClassInstance = new MyClass();
MyClassInstance.name = 'Joe';
MyClassInstance.name = 'Bob';


//Cual es  resultado de ejecuta las siguientes sentencias y porque?


console.log(MyClassInstance.name); //Nos imprime el name de Bob, devido a el get y set 
console.log(MyClassInstance.names_); //En este caso nos imprime todo el arreglo de names_ donde stan los dos nombres de Joe y bob