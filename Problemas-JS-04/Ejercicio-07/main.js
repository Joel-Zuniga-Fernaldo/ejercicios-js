//Dado el siguiente código

class Queue {
    constructor () {
        const list = [];

        this.enqueue = function (type) {
            list.push(type);
            return type;
        };

        this.dequeue = function () {
            return list.shift();
        };
    }
}

//Cual es el resultado de ejecuta las siguientes sentencias y porque?

var q = new Queue;

q.enqueue(9);
q.enqueue(8);
q.enqueue(7);


console.log(q.dequeue()); //Nos muestra el valor de enqueue el primero segun nuestra clase Queue
console.log(q.dequeue());//Nos muestra el segundo valor de enqueue segun la clase Queue
console.log(q.dequeue()); //nos manda el tercer valor de enqueue de acuerdo a nuestra clase 
console.log(q); //nos imprime todo lo que tenemos en nuestra clase Queue junto con el constructor
console.log(Object.keys(q));//Aquí estamos substrallendo solo  el metodo de keys de la clase Queue en este caso las enqueue y dequeue
