var p = document.getElementsByTagName('p')[0];
var sec = 0;
var min = 0;
var hrs = 0;
var t;

function tick() {
    sec++;
    if (sec >= 60) {
        sec = 0;
        min++;
        if (min >= 60) {
            min = 0;
            hrs++;
        }
    }
}
function add() {
    tick();
    p.textContent = (hrs > 9 ? hrs : "0" + hrs + ' ' + 'h' + ' ')
        + ":" + ' ' + (min > 9 ? min : "0" + min + ' ' + 'min' + ' ')
        + ":" + ' '+ (sec > 9 ? sec : "0" + sec + ' ' + 'seg');
    timer();
}
function timer() {
    t = setTimeout(add, 1000);
}

timer();
start.onclick = timer;
